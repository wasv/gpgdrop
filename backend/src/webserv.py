from flask import Flask, abort, request, Response
from botocore.exceptions import ClientError

from .util import *

import os

app = Flask(__name__)

@app.route("/", methods=['GET'])
def index():
    return "API Root"

@app.route("/drops", methods=['GET'])
def drops():
    objs = data_bucket.objects.all()
    return repr([entry.key for entry in objs])

@app.route("/drops/<path:path>", methods=['GET','POST'])
def file_op(path):
    if request.method == 'GET':
        objs = list(data_bucket.objects.filter(Prefix=path))
        paths = [entry.key for entry in objs]

        # Search for exact match of path.
        if path in paths:
            objs = list(filter(lambda entry: entry.key == path, objs))

        # If no single match found, return list of possible matches.
        if len(objs) > 1:
            return repr(paths)

        if len(objs) == 0:
            return "No such drop.", 404

        # If single match found, get it and return body.
        obj = objs[0].get()
        body = obj['Body'].read().decode('utf8')
        return Response(body, mimetype='text/plain')

    elif request.method == 'POST':
        write_keys = list_keys('write_keys')

        body = request.get_data(as_text=True)
        vres = gpg.verify(body)

        # If public key is not yet available, fetch it and retry.
        if vres.fingerprint:
            if vres.fingerprint in write_keys:
                import_key('write_keys',vres.fingerprint)
                vres = gpg.verify(body)
            else:
                return "Unauthorized Key | "+vres.fingerprint
        else:
            return "Bad Signature"


        if vres.valid:
            try:
                data_bucket.put_object(Key=path,Body=body,
                                           Metadata={'signed-by':vres.fingerprint})
            except ClientError as e:
                return "Invalid Path"
            return "Valid Signature"
        else:
            return "Bad Signature"
    else:
        abort(405)

@app.before_first_request
def init_s3():
    if data_bucket not in s3.buckets.all():
        data_bucket.create()
    if conf_bucket not in s3.buckets.all():
        conf_bucket.create()
