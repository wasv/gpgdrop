import gnupg
import boto3

import os
import io

s3 = boto3.resource('s3',
                  endpoint_url='http://storage:9000',
                  aws_access_key_id=os.environ['MINIO_ACCESS_KEY'],
                  aws_secret_access_key=os.environ['MINIO_SECRET_KEY'],
                  region_name='us-east-1')

gpg = gnupg.GPG(gnupghome="/app/gnupg", verbose=True)

DATA_BUCKET_NAME = os.getenv('DATA_BUCKET_NAME',"gpgdata")
data_bucket = s3.Bucket(DATA_BUCKET_NAME)

CONF_BUCKET_NAME = os.getenv('CONF_BUCKET_NAME',"gpgconf")
conf_bucket = s3.Bucket(CONF_BUCKET_NAME)

def import_key(prefix, fingerprint):
    path = prefix +'/'+ fingerprint
    print(path)
    key_data = io.BytesIO()
    conf_bucket.download_fileobj(path, key_data)
    gpg.import_keys(key_data.getvalue().decode('utf8'))
    gpg.trust_keys(fingerprint, 'TRUST_ULTIMATE')

def list_keys(prefix):
    objs = list(conf_bucket.objects.filter(Prefix=prefix))
    keys = [entry.key.split('/')[-1] for entry in objs]
    return keys
