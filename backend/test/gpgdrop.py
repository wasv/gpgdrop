# mkdir app
# apk add gnupg python3
# pip3 install python-gnupg

import gnupg
gpg = gnupg.GPG(gnupghome="/app/gnupg")
gpg.recv_keys('hkps://hkps.pool.sks-keyservers.net','EE3B571883FB1ACE9303289421A9CEB09A25B4F3')
res = gpg.trust_keys(fingerprints="A2BF35AA754A652C81978C7C958C184BDAC8F7AB", trustlevel = "TRUST_ULTIMATE")
v = gpg.verify_file(open("/app/test.txt", "rb"))
print(v.trust_text)
